import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {WikiComponent} from './wiki/wiki.component';
import {DownloadComponent} from './download/download.component';
import {DevelopComponent} from './develop/develop.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'wiki', component: WikiComponent},
  { path: 'download', component: DownloadComponent},
  { path: 'develop', component: DevelopComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
