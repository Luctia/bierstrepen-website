import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AnimateOnScrollModule} from 'ng2-animate-on-scroll';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WikiComponent } from './wiki/wiki.component';
import { NavComponent } from './nav/nav.component';
import { DevelopComponent } from './develop/develop.component';
import { DownloadComponent } from './download/download.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WikiComponent,
    NavComponent,
    DevelopComponent,
    DownloadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AnimateOnScrollModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
